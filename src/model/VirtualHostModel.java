package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import kernel.adapter.MenuItemAdapter;

public class VirtualHostModel extends Model implements MenuItemAdapter {
    public static final long serialVersionUID = 1001;
    private String serverAlias = "";
    private String serverName = "";
    private String documentRoot = "";
    private String ip = "";
    private String port = "";
    private HashMap<String, String> enviromentVars;
    private String direcotryPath = "";
    private ArrayList<String> allowOverride;

    public VirtualHostModel() {
        super();
        this.enviromentVars = new HashMap<>();
        this.allowOverride = new ArrayList<>();
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    public void setServerAlias(String alias) {
        this.serverAlias = alias;
        this.touch();
    }

    public String getServerAlias() {
        return this.serverAlias;
    }

    public void setServerName(String name) {
        this.serverName = name;
        this.touch();
    }

    public String getServerName() {
        return this.serverName;
    }

    public String getDocumentRoot() {
        return this.documentRoot;
    }

    public void setDocumentRoot(String documentRoot) {
        this.documentRoot = documentRoot;
        this.touch();
    }

    public void setIp(String value) {
        this.ip = value;
        this.touch();
    }

    public String getIp() {
        return this.ip;
    }

    public void setPort(String value) {
        this.port = value;
        this.touch();
    }

    public String getPort() {
        return this.port;
    }

    public void setDirectoryPath(String value) {
        this.direcotryPath = value;
        this.touch();
    }

    public String getDirectoryPath() {
        return this.direcotryPath;
    }

    public void addEnviromentVar(String name, String value) {
        this.enviromentVars.put(name, value);
        this.touch();
    }

    public Map<String, String> getEnviromentVars() {
        return this.enviromentVars;
    }

    public void setAllowOverride(ArrayList<String> allowOverride) {
        this.allowOverride = allowOverride;
    }

    public void setAllowOverride(String allowOverride) {
        String[] values = allowOverride.split(" ");
        this.setAllowOverride(new ArrayList<>(Arrays.asList(values)));
    }

    public List<String> getAllowOverride() {
        return this.allowOverride;
    }

    public boolean equals(Object object) {
        VirtualHostModel vhost = (VirtualHostModel) object;
        boolean result = true;
        List<Object> myObjects = this.getEqualsObjects();
        List<Object> otherobjects = vhost.getEqualsObjects();
        for (int i = 0; i < myObjects.size(); i++) {
            if (myObjects.get(i).equals(otherobjects.get(i))) {
                continue;
            }
            result = false;
        }
        return result;
    }

    public List<Object> getEqualsObjects() {
        return Arrays.asList(new Object[]{
            this.getDirectoryPath(),
            this.getDocumentRoot(),
            this.getServerAlias(),
            this.getServerName(),
            this.getIp(),
            this.getPort()
        });
    }

    public String toString() {
        String enviromentVars = "";
        Iterator<String> i = this.enviromentVars.keySet().iterator();
        while (i.hasNext()) {
            String varName = i.next();
            enviromentVars += String.format("\tSetEnv %s %s%n", varName, this.enviromentVars.get(varName));
        }

        String direcotory = "";
        if (!this.direcotryPath.isEmpty()) {
            String allowOverrideValue = this.getAllowOverride().stream().collect(Collectors.joining(" "));
            if (allowOverrideValue.isEmpty()) {
                allowOverrideValue = "None";
            }
            String directoryResultFormat = "\t<Directory %s>%n" +
                "\t\tAllowOverride %s%n" +
                "\t\tRequire all granted" +
                "\t</Direcotory>%n";
            direcotory = String.format(directoryResultFormat, this.getDirectoryPath(), allowOverrideValue);
        }

        String resultFormat = "# %s%n" + 
            "<VirtualHost %s:%s>%n" +
            "\tDocumentRoot %s%n" +
            "\tServerName %s%n" +
            "\tServerAlias %s%n";
        resultFormat += !enviromentVars.isEmpty() ? "%n%s" : "%s";
        resultFormat += "%s";

        resultFormat += "</VirtualHost>"; 

        String ip = this.getIp().isEmpty() ? "*" : this.getIp();
        String port = this.getPort().isEmpty() ? "80" : this.getPort();

        return String.format(resultFormat, 
            this.getServerAlias(), 
            ip, port,
            this.getDocumentRoot(), 
            this.getServerName(), 
            this.getServerAlias(), 
            enviromentVars,
            direcotory);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        VirtualHostModel clone = (VirtualHostModel) super.clone();
        clone.setServerAlias(new String(this.getServerAlias()));
        clone.setServerName(new String(this.getServerName()));
        clone.setDocumentRoot(new String(this.getDocumentRoot()));
        clone.setDirectoryPath(new String(this.getDirectoryPath()));
        clone.setIp(new String(this.getIp()));
        clone.setPort(new String(this.getPort()));
        return clone;
    }

    @Override
    public String getMenuText() {
        return this.getServerAlias();
    }

    @Override
    public Object getMenuValue() {
        return this;
    }
}
