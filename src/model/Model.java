package model;

import java.io.Serializable;
import java.util.ArrayList;

import facade.Provider;
import service.EventEmitter;
import service.EventService;
import service.EventService.EventListener;
import util.generator.IncrementalGenerator;
import util.modelbehavior.AppStateSerializable;

public abstract class Model implements Cloneable, AppStateSerializable {
    protected boolean dirty;
    protected boolean touched;
    protected int id;
    protected Object prototype;
    protected transient EventEmitter eventEmitter;
    public static IncrementalGenerator generator;

    public Model() {
        this._initialize();
        this.id = Model.generator.nextId();
    }

    public void initialize() {
        this.reset();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void reset() {
        this.dirty = false;
        this.touched = false;
        try {
            this.prototype = this.clone();
        } catch (CloneNotSupportedException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void _initialize() {
        EventService eventService = (EventService) Provider.get("service.event");
        this.eventEmitter = eventService.createEmitter();
    }

    @Override
    public void afterDeserialize() {
        this._initialize();
    }

    @Override
    public void beforeSerialize() {
        
    }

    protected void touch() {
        this.touched = true;
        //if (this.eventEmitter)
        this.eventEmitter.emit("touch", this);
    }

    public boolean isTouched() {
        return this.touched;
    }

    public void onTouch(EventListener listener) {
        this.eventEmitter.on("touch", listener);
    }

    public void offTouch(EventListener listener) {
        this.eventEmitter.off("touch", listener);
    }

    public boolean isDirty() {
        if (this.isTouched()) {
            this.dirty = !this.equals(this.prototype);
            this.touched = false;
        }
        return this.dirty;
    }

    public EventEmitter getEmitter() {
        return this.eventEmitter;
    }
}