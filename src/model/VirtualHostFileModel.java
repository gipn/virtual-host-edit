package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kernel.adapter.MenuItemAdapter;
import service.EventService.EventListener;

public class VirtualHostFileModel extends Model implements MenuItemAdapter {
    public static final long serialVersionUID = 1002;
    private ArrayList<VirtualHostModel> models;
    private String name;
    private File file;
    private transient OnVirtualHostModelTouch onModelTouch;

    public VirtualHostFileModel(File file) {
        this(file.getName());
        this.file = file;
        this._initialize();
    }

    public VirtualHostFileModel(String name) {
        super();
        this.models = new ArrayList<>();
        this.setName(name);
    }

    private void _initialize() {
        this.onModelTouch = new OnVirtualHostModelTouch(this);
    }

    @Override
    public void afterDeserialize() {
        super.afterDeserialize();
        this._initialize();
        for (VirtualHostModel model : this.models) {
            model.afterDeserialize();
        }
    }

    @Override
    public void reset() {
        super.reset();
        for (VirtualHostModel model : models) {
            model.reset();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public List<VirtualHostModel> getChildren() {
        return this.models;
    }

    public void removeHost(VirtualHostModel model) {
        model.offTouch(this.onModelTouch);
        this.getChildren().remove(model);
        this.eventEmitter.emit("change", this);
    }

    public void addHost(VirtualHostModel model) {
        model.onTouch(this.onModelTouch);
        this.getChildren().add(model);
        this.eventEmitter.emit("change", this);
    }

    public void addAllHosts(List<VirtualHostModel> hosts) {
        for (VirtualHostModel host : hosts) {
            this.addHost(host);
        }
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return this.file;
    }

    public String toString() {
        String result = "";
        for (VirtualHostModel model : this.getChildren()) {
            result += String.format("%s%n%n", model.toString());
        }
        return result;
    }

    @Override
    public boolean isDirty() {
        boolean dirty = false;
        for (VirtualHostModel model : this.getChildren()) {
            if (!model.isDirty()) {
                continue;
            }
            dirty = true;
        }
        return dirty;
    }

    public class OnVirtualHostModelTouch implements EventListener {
        private VirtualHostFileModel fileModel;

        public OnVirtualHostModelTouch(VirtualHostFileModel fileModel) {
            this.fileModel = fileModel;
        }

        @Override
        public void handle(Object data) {
            this.fileModel.touch();
        }
    }

    @Override
    public String getMenuText() {
        return this.getName();
    }

    @Override
    public Object getMenuValue() {
        return this;
    }
}