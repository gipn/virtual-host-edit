import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import component.Component;
import component.ScannerComponent;
import component.VirtualHostComponent;
import facade.Provider;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import service.AppStateService;
import service.AutoloaderService;
import service.EventService;
import service.FileScannerService;
import service.FormService;
import service.KeyboardShortcutService;
import service.MenuService;
import service.ModelService;
import service.ParserService;
import service.RouterService;
import service.Service;
import service.VirtualHostService;
import util.filescanner.LinuxApacheScanner;
import util.filescanner.WindowsScanner;
 
public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        /**
         * Register services
         */
        AutoloaderService autoloader = new AutoloaderService();
        FormService formService = new FormService();
        EventService eventService = new EventService();
        AppStateService appStateService = new AppStateService();
        KeyboardShortcutService keyboardShortcutService = new KeyboardShortcutService();
        MenuService menuService = new MenuService();
        VirtualHostService virtualHostService = new VirtualHostService();
        FileScannerService fileScannerService = new FileScannerService();
        RouterService routerService = new RouterService();

        Map<String, Service> services = new HashMap<>();
        services.put("autoloader", autoloader);
        services.put("service.app.state", appStateService);
        services.put("service.event", eventService);
        services.put("service.router", routerService);
        services.put("service.model", new ModelService());
        services.put("service.parser", new ParserService());
        services.put("service.virtual.host", virtualHostService);
        services.put("service.menu", menuService);
        services.put("service.form", formService);
        services.put("service.keyboard.shortcut", keyboardShortcutService);
        services.put("service.file.scanner", fileScannerService);
        
        Iterator<String> i = services.keySet().iterator();
        while (i.hasNext()) {
            String name = i.next();
            Provider.register(name, services.get(name));
        }

        /**
         * autoload services into services
         */
        for (Service service : services.values()) {
            autoloader.loadServices(service);
        }
        
        /**
         * initialize services
         */
        for (Service service : services.values()) {
            service.initialize();
        }
        Component virtualHost = new VirtualHostComponent();
        Component scanner = new ScannerComponent();
        routerService.addRoute("*", virtualHost);
        routerService.addRoute("virtual-host", virtualHost);
        routerService.addRoute("scan", scanner);

        Parent root = FXMLLoader.load(this.getClass().getResource("view/main_layout.fxml"));

        Scene scene = new Scene(root, 900, 700);
        scene.getStylesheets().add(this.getClass().getResource("resources/css/layout.css").toExternalForm());
        /**
         * Boot services
         */
        formService.boot(scene);
        keyboardShortcutService.boot(scene);

        /**
         * Configuration phase
         */
        keyboardShortcutService.add("save", new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN), () -> {
            virtualHostService.save(menuService.getActive());
        });
        fileScannerService.addScanner(new LinuxApacheScanner());
        fileScannerService.addScanner(new WindowsScanner(Paths.get("/xampp/apache/conf/extra/httpd-vhosts.conf").toString()));
        fileScannerService.addScanner(new WindowsScanner(Paths.get("/xampp/apache/conf/extra/httpd-ssl.conf").toString()));
        fileScannerService.addScanner(new WindowsScanner(Paths.get("/MAMP/bin/apache/conf/extra/httpd-vhosts.conf").toString()));
        fileScannerService.addScanner(new WindowsScanner(Paths.get("/MAMP/bin/apache/conf/extra/httpd-ssl.conf").toString()));

        primaryStage.setTitle("Virtual host editor");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            eventService.emit("window.close", event);
        });

        /**
         * After render
         */
        for (Service service : services.values()) {
            service.afterRender();
        }
    }
}