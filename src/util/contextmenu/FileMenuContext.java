package util.contextmenu;

import facade.Provider;
import javafx.event.ActionEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import model.VirtualHostFileModel;
import model.VirtualHostModel;
import service.MenuService;
import service.VirtualHostService;

public class FileMenuContext implements ContextMenuBuilder {
    private MenuService menuService;
    private VirtualHostService virtualHostService;

    public FileMenuContext() {
        this.menuService = (MenuService) Provider.get("service.menu");
        this.virtualHostService = (VirtualHostService) Provider.get("service.virtual.host");
    }
    public void build(ContextMenu menu, Object object) {
        VirtualHostFileModel model = (VirtualHostFileModel) object;

        MenuItem remove = new MenuItem("remove");
        remove.setOnAction((ActionEvent event) -> {
            this.virtualHostService.remove(model);
        });

        MenuItem add = new MenuItem("add host");
        add.setOnAction((ActionEvent event) -> {
            VirtualHostModel host = new VirtualHostModel();
            host.setServerAlias("localhost");
            host.initialize();
            model.addHost(host);
            this.menuService.setActive(model.getChildren().get(model.getChildren().size() - 1));
        });

        menu.getItems().add(remove);
        menu.getItems().add(add);
    }
}