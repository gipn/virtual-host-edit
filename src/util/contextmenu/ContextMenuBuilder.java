package util.contextmenu;

import javafx.scene.control.ContextMenu;

public interface ContextMenuBuilder {
    public void build(ContextMenu menu, Object model);
}