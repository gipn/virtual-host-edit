package util.contextmenu;

import facade.Provider;
import javafx.event.ActionEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import model.VirtualHostModel;
import service.MenuService;
import service.VirtualHostService;

public class HostMenuContext implements ContextMenuBuilder {
    private VirtualHostService virtualHostService;

    public HostMenuContext() {
        this.virtualHostService = (VirtualHostService) Provider.get("service.virtual.host");
    }

    @Override
    public void build(ContextMenu menu, Object object) {
        final VirtualHostModel model = (VirtualHostModel) object;

        MenuItem remove = new MenuItem("Remove");
        remove.setOnAction((ActionEvent event) -> {
            this.virtualHostService.remove(model);
        });

        MenuItem save = new MenuItem("Save");
        save.setOnAction((ActionEvent event) -> {
            this.virtualHostService.save(model);
        });

        menu.getItems().add(save);
        menu.getItems().add(remove);
    }
}