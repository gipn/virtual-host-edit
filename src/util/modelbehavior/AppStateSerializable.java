package util.modelbehavior;

import java.io.Serializable;

public interface AppStateSerializable extends Serializable {
    public void afterDeserialize();
    public void beforeSerialize();
}