package util.generator;

import java.util.ArrayList;

import util.modelbehavior.AppStateSerializable;

public class IncrementalGenerator implements AppStateSerializable {

    private int currentId = 0;
    private ArrayList<Integer> userIds;

    public IncrementalGenerator() {
        this.userIds = new ArrayList<>();
    }

    public int nextId() {
        while (true) {
            if (this.currentId == Integer.MAX_VALUE) {
                this.currentId = 0;
            }
            this.currentId++;
            if (!this.userIds.contains(this.currentId)) {
                break;
            }
        }
        
        this.userIds.add(this.currentId);
        return this.currentId;
    }

    public void free(Integer id) {
        this.userIds.remove(id);
    }

    @Override
    public void afterDeserialize() {
        
    }

    @Override
    public void beforeSerialize() {
        
    }
}