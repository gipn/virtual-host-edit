package util.parser.pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.VirtualHostModel;

public class DirectoryStart extends ParserPattern {

    public DirectoryStart() {
        super("^\\s*<Directory (.+)>$");
    }

    public void setModel(String line, VirtualHostModel model) {
        Matcher mather = this.getGroups(line);
        model.setDirectoryPath(mather.group(1));
    }
}