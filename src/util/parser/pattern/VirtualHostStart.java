package util.parser.pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.VirtualHostModel;

public class VirtualHostStart extends ParserPattern {

    public VirtualHostStart() {
        super("^\\s*<VirtualHost (.+):(\\d+)>$");
    }

    public void setModel(String line, VirtualHostModel model) {
        Matcher mather = this.getGroups(line);
        model.setIp(mather.group(1));
        model.setPort(mather.group(2));
    }
}