package util.parser.pattern;

import java.util.regex.Matcher;
import model.VirtualHostModel;

public class ServerAliasProperty extends ParserPattern {
    public ServerAliasProperty() {
        super("^\\s*ServerAlias (?<value>.+)$");
    }

    public void setModel(String line, VirtualHostModel model) {
        Matcher matcher = this.getGroups(line);
        model.setServerAlias(matcher.group("value"));
    }
}