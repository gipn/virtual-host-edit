package util.parser.pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.VirtualHostModel;

public class VirtualHostEnd extends ParserPattern {

    public VirtualHostEnd() {
        super("^\\s*</VirtualHost>$");
    }

    public void setModel(String line, VirtualHostModel model) {}
}