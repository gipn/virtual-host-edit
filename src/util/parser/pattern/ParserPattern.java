package util.parser.pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.VirtualHostModel;

abstract public class ParserPattern {
    protected Pattern pattern;

    public ParserPattern(String stringPattern) {
        this.pattern = Pattern.compile(stringPattern);
    }

    protected Matcher getMatcher(String line) {
        return this.pattern.matcher(line);
    }

    protected Matcher getGroups(String line) {
        Matcher matcher = this.getMatcher(line);
        matcher.find();
        return matcher;
    }

    public boolean isMatch(String line) {
        Matcher matcher = this.getMatcher(line);
        return matcher.find();
    }

    abstract public void setModel(String line, VirtualHostModel model);
}