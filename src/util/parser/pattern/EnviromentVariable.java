package util.parser.pattern;

import java.util.regex.Matcher;
import model.VirtualHostModel;

public class EnviromentVariable extends ParserPattern {
    public EnviromentVariable() {
        super("^\\s*SetEnv (?<name>.+) (?<value>.+)$");
    }

    public void setModel(String line, VirtualHostModel model) {
        Matcher matcher = this.getGroups(line);
        model.addEnviromentVar(matcher.group("name"), matcher.group("value"));
    }
}