package util.parser.pattern;

import java.util.regex.Matcher;
import model.VirtualHostModel;

public class DocumentRootProperty extends ParserPattern {
    public DocumentRootProperty() {
        super("^\\s*DocumentRoot (?<value>.+)$");
    }

    public void setModel(String line, VirtualHostModel model) {
        Matcher matcher = this.getGroups(line);
        model.setDocumentRoot(matcher.group("value"));
    }
}