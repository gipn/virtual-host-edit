package util.parser.pattern;

import java.util.regex.Matcher;

import model.VirtualHostModel;

public class AllowOverride extends ParserPattern {
    public AllowOverride() {
        super("^\\s*AllowOverride (?<name>.+)$");
    }

    public void setModel(String line, VirtualHostModel model) {
        Matcher matcher = this.getGroups(line);
        model.setAllowOverride(matcher.group("name"));
    }
}