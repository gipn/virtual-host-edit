package util.parser.pattern;

import java.util.regex.Matcher;
import model.VirtualHostModel;

public class ServerNameProperty extends ParserPattern {
    public ServerNameProperty() {
        super("^\\s*ServerName (?<value>.+)$");
    }

    public void setModel(String line, VirtualHostModel model) {
        Matcher matcher = this.getGroups(line);
        model.setServerName(matcher.group("value"));
    }
}