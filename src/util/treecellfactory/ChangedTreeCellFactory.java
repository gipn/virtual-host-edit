package util.treecellfactory;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeCell;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import kernel.adapter.MenuItemAdapter;
import model.Model;
import model.VirtualHostModel;

public class ChangedTreeCellFactory extends TreeCell<MenuItemAdapter> {
    private Node node;
    private Text text;
    private Circle circle;
    private MenuItemAdapter adapter;

    public ChangedTreeCellFactory() {
        HBox box = new HBox();
        VBox textBox = new VBox();
        this.text = new Text();
        this.circle = new Circle();
        this.circle.setRadius(4.0f);
        this.circle.setVisible(false);
        HBox.setHgrow(textBox, Priority.ALWAYS);
        textBox.getChildren().add(this.text);

        box.setAlignment(Pos.CENTER);
        box.getChildren().add(textBox);
        box.getChildren().add(this.circle);
        this.node = box;
    }

    @Override
    public void updateItem(MenuItemAdapter adapter, boolean empty) {
        super.updateItem(adapter, empty);
        if (empty) {
            this.setText(null);
            this.setGraphic(null);
            return;
        } else {
            this.setGraphic(this.node);
        }        

        if (adapter == this.adapter) {
            return;
        }
        this.adapter = adapter;

        if (this.adapter != null) {
            Model model = (Model) adapter.getMenuValue();
            if (model != null) {
                model.onTouch((event) -> {
                    this.refresh(this.adapter);
                });
            }
        }
        
        this.refresh(adapter);
    }

    private void refresh(MenuItemAdapter adapter) {
        Model model = null;;
        if (adapter != null && adapter.getMenuValue() != null) {
            model = (Model) adapter.getMenuValue();
        } else {
            this.setVisible(false);
        }
        this.circle.setVisible((model != null) ? model.isDirty() : false);
        this.text.setText((adapter != null) ? adapter.getMenuText() : "");
    }
}