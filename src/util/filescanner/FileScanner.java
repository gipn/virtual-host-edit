package util.filescanner;

import java.io.File;
import java.util.List;

public interface FileScanner {
    public List<File> scan();
}