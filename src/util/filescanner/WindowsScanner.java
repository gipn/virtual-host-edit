package util.filescanner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WindowsScanner implements FileScanner {
    private File vhostFile;

    public WindowsScanner(String vhostFile) {
        this.vhostFile = new File(vhostFile);
    }

    protected boolean shouldScan() {
        return true;
    }

    protected File getVhostFile() {
        return this.vhostFile;
    }

    @Override
    public List<File> scan() {
        List<File> files = new ArrayList<>();
        if (!this.shouldScan()) {
            return files;
        }
        File vhostFile = this.getVhostFile();
        if (!vhostFile.isFile()) {
            return files;
        }

        files.add(vhostFile);

        return files;
    }
}