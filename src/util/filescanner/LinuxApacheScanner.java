package util.filescanner;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LinuxApacheScanner implements FileScanner {

    private boolean shouldScan() {
        return true;
        // String osName = System.getProperty("os.name");
        // return osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") >= 0;
    }
    @Override
    public List<File> scan() {
        List<File> files = new ArrayList<>();
        if (!this.shouldScan()) {
            return files;
        }
        File sitesAvailable = Paths.get("/etc/apache2/sites-available").toFile();
        if (!sitesAvailable.exists()) {
            return files;
        }

        for (File file : sitesAvailable.listFiles()) {
            files.add(file);
        }

        return files;
    }
}