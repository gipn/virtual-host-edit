package util.componentbehavior;

public interface Responsive {
    public void onBreakpointActivate(String name);
}