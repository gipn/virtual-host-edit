package component;

import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import kernel.adapter.MenuItemAdapter;
import util.contextmenu.ContextMenuBuilder;

public class DynamicMenuContextComponent extends Component<Object> implements EventHandler<MouseEvent> {

    private ContextMenu contextMenu;
    private Map<Class, ContextMenuBuilder> menuBuilders;

    public void addBuilder(Class c, ContextMenuBuilder builder) {
        this.menuBuilders.put(c, builder);
    }

    @Override
    protected void build() {
        Map<String, String> map = new HashMap<>();
        this.menuBuilders = new HashMap<>();
        this.contextMenu = new ContextMenu();
        super.build();
    }

    @Override
    public void update(Object data) {
        
    }

    @Override
    public Object render() {
        return this.contextMenu;
    }

    @Override
    public void handle(MouseEvent event) {
        if (event.getButton() != MouseButton.SECONDARY) {
            this.contextMenu.hide();
            return;
        }
        this.contextMenu.getItems().clear();
        TreeView tree = (TreeView) event.getSource();
        TreeItem<MenuItemAdapter> item = (TreeItem<MenuItemAdapter>) tree.getSelectionModel().getSelectedItem();
        MenuItemAdapter adapter = item.getValue();
        if (!this.menuBuilders.containsKey(adapter.getMenuValue().getClass())) {
            return;
        }
        this.menuBuilders.get(adapter.getMenuValue().getClass()).build(this.contextMenu, adapter.getMenuValue());
        this.contextMenu.show((Node) event.getTarget(), event.getScreenX(), event.getScreenY());
    }
}