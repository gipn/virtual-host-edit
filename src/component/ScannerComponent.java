package component;

import java.io.File;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import kernel.anotations.Service;
import service.EventService;
import service.FileScannerService;
import service.MenuService;

public class ScannerComponent extends Component<Object> {
    private Text text;
    @Service public FileScannerService serviceFileScanner;
    @Service public MenuService serviceMenu;
    @Service public EventService serviceEvent;

    @Override
    public void initialize() {
        super.initialize();
        this.serviceEvent.on("menu.change", (event) -> {
            this.update();
        });
    }

    public void build() {
        this.container = new VBox();
        this.text = new Text("");
        this.container.getChildren().add(this.text);
        this.container.setPadding(new Insets(8.0, 16.0, 16.0, 8.0));
    }

    public void update(Object data) {
        if (data == null) {
            data = this.serviceFileScanner.scan();
        }
        this.container.getChildren().clear();
        List<File> files = (List<File>) data;        
        for (File f : files) {
            Component item = new FileScanItemComponent();
            item.initialize(f);
            this.container.getChildren().add((Node) item.render());
        }
    }
}