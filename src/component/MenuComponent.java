package component;

import service.*;
import model.*;
import service.EventService.EventListener;
import util.contextmenu.FileMenuContext;
import util.contextmenu.HostMenuContext;
import util.treecellfactory.ChangedTreeCellFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;

import facade.Provider;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import kernel.adapter.MenuItemAdapter;
import kernel.adapter.StringMenuItemAdapter;
import kernel.anotations.Component;
import kernel.anotations.Service;

public class MenuComponent extends component.Component<Object> {

    private TreeView<MenuItemAdapter> treeView;
    private TreeItem<MenuItemAdapter> rootItem;
    @Service public FormService serviceForm;
    @Service public EventService serviceEvent;
    @Service public MenuService serviceMenu;
    @Service public RouterService serviceRouter;
    @Component private DynamicMenuContextComponent menuContext;

    public MenuComponent() {
        super();
        this.serviceEvent.on("menu.change", (data) -> {
            this.update(data);
        });
        this.serviceEvent.on("menu.active.change", (data) -> {
            VirtualHostModel model = (VirtualHostModel) data;
            this.select(model);
        });
    }

    @Override
    public void initialize() {
        super.initialize();
        this.update(this.serviceMenu.getMenuItems());
        this.select(this.serviceMenu.getActive());
    }

    private void select(VirtualHostModel model) {
        TreeItem<MenuItemAdapter> item = this.findItem(this.rootItem, model);
        this.select(item);
    }

    private void select(TreeItem<MenuItemAdapter> item) {
        if (item == null) {
            return;
        }
        this.expand(item);
        int index = this.treeView.getRow(item);
        this.treeView.getSelectionModel().select(index);
    }


    private TreeItem<MenuItemAdapter> findItem(TreeItem<MenuItemAdapter> inItem, VirtualHostModel model) {
        TreeItem<MenuItemAdapter> result = null;
        for (TreeItem<MenuItemAdapter> item : inItem.getChildren()) {
            if (item.isLeaf()) {
                if (item.getValue().getMenuValue() != model) {
                    continue;
                }
                result = item;
            }
            else {
                result = this.findItem(item, model);
            }
            if (result == null) {
                continue;
            }
            return result;
        }
        return result;
    }

    private void expand(TreeItem<MenuItemAdapter> item) {
        while (item.getParent() != null) {
            item.getParent().setExpanded(true);
            item = item.getParent();
        }
    }

    @Override
    protected void build() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(16.0, 0.0, 16.0, 0.0));
        vbox.getStyleClass().add("left-pane");

        this.rootItem = new TreeItem<>(new StringMenuItemAdapter("Virtula hosts"));
        this.rootItem.setExpanded(true);
        this.treeView = new TreeView<>(this.rootItem);
        this.treeView.setShowRoot(false);
        this.treeView.setCellFactory((TreeView<MenuItemAdapter> p) ->  {
            return new ChangedTreeCellFactory();
        });
        this.treeView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
                TreeItem<MenuItemAdapter> item = (TreeItem<MenuItemAdapter>) newValue;
                if (item == null) {
                    // this.serviceMenu.setActive(null);
                    return;
                }
                MenuItemAdapter adapter = item.getValue();
                if (!(adapter.getMenuValue() instanceof VirtualHostModel)) {
                    return;
                }
                this.serviceRouter.route("virtual-host");
                this.serviceMenu.setActive((VirtualHostModel) adapter.getMenuValue());
            });
        
        this.menuContext.initialize();
        this.menuContext.addBuilder(VirtualHostFileModel.class, new FileMenuContext());
        this.menuContext.addBuilder(VirtualHostModel.class, new HostMenuContext());
        this.treeView.setOnMouseClicked(this.menuContext);

        Button addButton = new Button("Otvor");
        addButton.setOnAction((event) -> {
            File selectedFile = this.serviceForm.openFile("Otvor vhost súbor");
            this.serviceMenu.addFile(selectedFile);
        });

        Button scanButton = new Button("Scan");
        scanButton.setOnAction((event) -> {
            this.serviceRouter.route("scan");
        });

        HBox actions = new HBox(8.0);
        actions.getChildren().add(addButton);
        actions.getChildren().add(scanButton);

        vbox.getChildren().add(treeView);
        vbox.getChildren().add(actions);
        this.container = vbox;
    }

    private TreeItem<MenuItemAdapter> findOrCreate(VirtualHostFileModel model) {
        for (TreeItem<MenuItemAdapter> item : this.rootItem.getChildren()) {
            if (item.getValue().getId() != model.getId()) {
                continue;
            }
            return item;
        }
        MenuItemComponent item = new MenuItemComponent();
        item.initialize();
        item.update(model);
        model.getEmitter().on("change", (event) -> {
            item.update((VirtualHostFileModel) event);
        });
        return (TreeItem<MenuItemAdapter>) item.render();
    }

    @Override
    public void update(Object object) {
        List<VirtualHostFileModel> fileModels = (List<VirtualHostFileModel>) object;
        if (fileModels == null) {
            return;
        }
        List<TreeItem<MenuItemAdapter>> newItems = new ArrayList<>();
        
        for (VirtualHostFileModel fileModel : fileModels) {
            newItems.add(this.findOrCreate(fileModel));
        }
        this.rootItem.getChildren().clear();
        this.rootItem.getChildren().addAll(newItems);
    }
}