package component;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.GroupLayout.Alignment;

import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import kernel.anotations.Component;
import kernel.anotations.Service;
import model.VirtualHostModel;
import service.EventService;
import service.FormService;
import service.MenuService;
import service.EventService.EventListener;
import util.componentbehavior.Responsive;

public class VirtualHostComponent extends component.Component<VirtualHostModel> implements Responsive {

    private VirtualHostModel currentModel = null;
    private TextField aliasName;
    private TextField serverName;
    @Component private PathChooserComponent documentRoot;
    private TextArea enviromentVariables;
    private TextField port;
    private TextField ip;
    @Component private PathChooserComponent direcotryPath;
    @Component private MultichoiceAllCheckboxComponent allowOverride;
    private ResponsiveComponent responsiveComponent;
    @Service public FormService serviceForm;
    @Service public MenuService serviceMenu;

    private List<String> allowOverrideChoices;
    private List<VBox> blocks;

    public void initialize() {
        this.blocks = new ArrayList<>();

        this.allowOverrideChoices = new ArrayList<>();
        this.allowOverrideChoices.add("AuthConfig");
        this.allowOverrideChoices.add("FileInfo");
        this.allowOverrideChoices.add("Indexes");
        this.allowOverrideChoices.add("Limit");

        this.serviceEvent.on("menu.active.change", (data) -> {
            this.update((VirtualHostModel) data);
        });
        super.initialize();
        this.update(this.serviceMenu.getActive());
    }

    @Override
    protected void build() {
        this.ip = new TextField();
        this.port = new TextField();
        HBox listeningAddress = new HBox();
            this.ip.textProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
                if (this.currentModel == null) {
                    return;
                }
                this.currentModel.setIp(newValue.toString());
            });
            this.port.textProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
                if (this.currentModel == null) {
                    return;
                }
                this.currentModel.setPort(newValue.toString());
            });
        listeningAddress.getChildren().add(this.ip);
        listeningAddress.getChildren().add(this.port);

        this.aliasName = new TextField();
        this.aliasName.textProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
            if (this.currentModel == null) {
                return;
            }
            this.currentModel.setServerAlias(newValue.toString());
        });
        this.serverName = new TextField();
        this.serverName.textProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
            if (this.currentModel == null) {
                return;
            }
            this.currentModel.setServerName(newValue.toString());
        });
        this.documentRoot.initialize();
        this.documentRoot.textProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
            if (this.currentModel == null) {
                return;
            }
            this.currentModel.setDocumentRoot(newValue.toString());
        });
        this.enviromentVariables = new TextArea();

        this.direcotryPath.initialize();
        this.direcotryPath.textProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
            if (this.currentModel == null) {
                return;
            }
            this.currentModel.setDirectoryPath(newValue.toString());
        });
        
        this.allowOverride.initialize(null, this.allowOverrideChoices);
        this.allowOverride.addListener((ObservableValue<?> Observable, Object oldValue, Object newValue) -> {
            if (this.currentModel == null) {
                return;
            }
            this.currentModel.setAllowOverride((ArrayList<String>) newValue);
        });

        this.blocks.add(new VBox());
        this.blocks.add(new VBox());
        VBox block = this.blocks.get(0);
        block.getChildren().add(new Text("Virtual host"));
        block.getChildren().add(new Label("IP and port"));
        block.getChildren().add(listeningAddress);
        block.getChildren().add(new Label("Alias alias"));
        block.getChildren().add(this.aliasName);
        block.getChildren().add(new Label("Server name"));
        block.getChildren().add(this.serverName);
        block.getChildren().add(new Label("Document root folder"));
        block.getChildren().add((Node) this.documentRoot.render());
        block.getChildren().add(new Label("Enviroment variables"));
        block.getChildren().add(this.enviromentVariables);
        block.setPadding(new Insets(16.0, 16.0, 16.0, 16.0));

        block = this.blocks.get(1);
        block.getChildren().add(new Text("Directory"));
        block.getChildren().add(new Label("Directory folder path"));
        block.getChildren().add((Node) this.direcotryPath.render());
        block.getChildren().add(new Label("Allow override"));
        block.getChildren().add((Node) this.allowOverride.render());
        block.setPadding(new Insets(16.0, 16.0, 16.0, 16.0));

        this.container = new VBox();
        //this.container.setPadding(new Insets(16.0, 16.0, 16.0, 16.0));

        this.responsiveComponent = new ResponsiveComponent(this.container);
        this.responsiveComponent.setName("mobile");
        this.responsiveComponent.addBreakpoint(800, "desktop");
        this.responsiveComponent.setResponsive(this);
        this.responsiveComponent.initialize();
    }

    @Override
    public void update(VirtualHostModel model) {
        if (model == null) {
            return;
        }
        this.currentModel = model;
        this.aliasName.setText(model.getServerAlias());
        this.serverName.setText(model.getServerName());
        this.documentRoot.update(model.getDocumentRoot());
        this.ip.setText(model.getIp());
        this.port.setText(model.getPort());
        String envVars = "";
        Iterator<String> i = model.getEnviromentVars().keySet().iterator();
        while (i.hasNext()) {
            String envName = i.next();
            envVars += String.format("%s %s%n", envName, model.getEnviromentVars().get(envName));
        }
        this.enviromentVariables.setText(envVars);
        this.direcotryPath.update(model.getDirectoryPath());
        this.allowOverride.update(model.getAllowOverride());
    }

    @Override
    public void onBreakpointActivate(String name) {
        Pane node;
        switch (name) {
            case "mobile":
                node = new VBox(4.0);
                node.getChildren().add(this.blocks.get(0));
                node.getChildren().add(new Separator());
                node.getChildren().add(this.blocks.get(1));
                break;
            default:
            case "desktop":
                node = new HBox(4.0);
                node.getChildren().add(this.blocks.get(0));
                node.getChildren().add(this.blocks.get(1));
                break;
        }
        this.container.getChildren().clear();
        this.container.getChildren().add(node);
    }
}