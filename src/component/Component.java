package component;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import facade.Provider;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import service.AutoloaderService;
import service.EventService;
import kernel.anotations.Service;

public abstract class Component<T> {

    private AutoloaderService autoloader;
    protected Pane container;
    @Service public EventService serviceEvent;
    private boolean isInitialize;

    public Component() {
        this.autoloader = (AutoloaderService) Provider.get("autoloader");
        this.autoloader.loadServices(this);
        this.autoloader.loadComponents(this);
        this.isInitialize = false;
    }

    public void initialize() {
        this.initialize(null);
    }

    public void initialize(T model) {
        if (isInitialize) {
            return;
        }
        this.build();
        this.update(model);
        this.isInitialize = true;
    }

    protected void build() {

    }

    public void update() {
        this.update(null);
    }
    public abstract void update(T data);
    
    public Object render() {
        this.initialize();
        return this.container;
    }

    // public void addComponent(Component component) {
    //     this.container.getChildren().add((Node) component.render());
    // }

    public Scene getScene() {
        return this.container.getScene();
    }
}