package component;

import java.io.File;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import kernel.anotations.Service;
import service.MenuService;
import service.VirtualHostService;

public class FileScanItemComponent extends Component<File> {
    private Button addButton;
    private Text fileName;
    private Text addedText;
    private File file;
    @Service public VirtualHostService serviceVirtualHost;
    @Service public MenuService serviceMenu;

    @Override
    protected void build() {
        this.container = new HBox();
        HBox textBox = new HBox();
        this.fileName = new Text();
        this.addedText = new Text("V menu");
        this.addButton = new Button("Pridaj");
        this.addButton.getStyleClass().add("btn-primary");

        this.addButton.setOnAction((event) -> {
            this.serviceMenu.addFile(this.file);
            this.refresh();
        });

        HBox.setHgrow(textBox, Priority.ALWAYS);
        textBox.getChildren().add(this.fileName);
        this.container.getChildren().add(textBox);
        this.container.setPadding(new Insets(8.0, 0.0, 8.0, 0.0));
    }

    @Override
    public void update(File data) {
        this.file = data;
        this.refresh();
    }

    private void refresh() {
        if (this.file == null) {
            return;
        }
        this.fileName.setText(this.file.getAbsolutePath());
        System.out.println("UPDATE SYNC ITEM");
        System.out.println(this.serviceVirtualHost.has(this.file));
        if (this.serviceVirtualHost.has(this.file)) {
            this.container.getChildren().remove(this.addButton);
            this.container.getChildren().add(this.addedText);
        } else {
            this.container.getChildren().add(this.addButton);
            this.container.getChildren().remove(this.addedText);
        }
    }
}