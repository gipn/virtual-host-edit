package component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import util.componentbehavior.Responsive;

public class ResponsiveComponent {

    private List<Integer> breakpoints;
    private List<String> names;
    private Responsive responsive;
    private Pane pane;

    public ResponsiveComponent(Pane pane) {
        this.breakpoints = new ArrayList<>();
        this.names = new ArrayList<>();
        this.pane = pane;
        this.pane.widthProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
            if (this.responsive == null) {
                return;
            }
            Number newWidth = (Number) newValue;
            Number oldWidth = (Number) oldValue;
            Integer newRange[] = this.getRange(newWidth.intValue());
            Integer oldRange[] = this.getRange(oldWidth.intValue());
            if (newRange[0] == oldRange[0] && newRange[1] == newRange[1]) {
                return;
            }

            this.responsive.onBreakpointActivate(this.getName(newRange));
        });
    }

    public ResponsiveComponent setName(String name) {
        if (this.names.size() > this.breakpoints.size()) {
            this.names.remove(0);
        }
        this.names.add(0, name);
        return this;

    }

    public ResponsiveComponent addBreakpoint(Integer width, String name) {
        this.names.add(name);
        this.breakpoints.add(width);
        return this;
    }

    public ResponsiveComponent setResponsive(Responsive responsive) {
        this.responsive = responsive;
        return this;
    }

    private Integer[] getRange(Integer width) {
        Integer range[] = {0, 0};
        for (Integer breakpoint : this.breakpoints) {
            range[1] = breakpoint;
            if (breakpoint >= width) {
                break;
            }
            range[0] = range[1];
        }
        if (range[0] == range[1]) {
            range[1] = -1;
        }
        return range;
    }

    private String getName(Integer[] range) {
        Integer index = 0;
        if (range[1] == -1) {
            index = this.names.size() - 1;
        } else {
            for (Integer i = 0; i < this.breakpoints.size(); i++) {
                if (range[0] != this.breakpoints.get(i)) {
                    continue;
                }
                index = i;
            }
        }
        return this.names.get(index);
    }

    public void initialize() {
        this.fire(this.pane.widthProperty().getValue());
    }

    public void fire(Number width) {
        Integer range[] = this.getRange(width.intValue());
        this.responsive.onBreakpointActivate(this.getName(range));
    }
}