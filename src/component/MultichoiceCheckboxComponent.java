package component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;

public class MultichoiceCheckboxComponent extends Component<List<String>> {

    protected List<String> choices;
    protected List<CheckBox> checkboxes;
    protected List<ChangeListener> listeners;
    protected List<String> oldValue;

    public void initialize(List<String> model, List<String> choices) {
        this.listeners = new LinkedList<>();
        this.checkboxes = new ArrayList<>();
        this.oldValue = new ArrayList<>();
        this.choices = choices;
        this.initialize(model);
    }

    protected CheckBox createCheckbox(String text) {
        CheckBox checkbox = new CheckBox();
        checkbox.setPadding(new Insets(4.0, 8.0, 4.0, 8.0));
        checkbox.setText(text);
        return checkbox;
    }

    @Override
    protected void build() {
        FlowPane pane = new FlowPane();
        this.checkboxes.clear();

        for (String name : this.choices) {
            CheckBox checkbox = this.createCheckbox(name);
            checkbox.selectedProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
                this.onChange(observable, oldValue, newValue);
            });
            this.checkboxes.add(checkbox);
            pane.getChildren().add(checkbox);
        }

        this.container = pane;
    }

    @Override
    public void update(List<String> data) {
        if (data == null) {
            data = new ArrayList<>();
        }
        for (CheckBox checkBox : this.checkboxes) {
            checkBox.setSelected(data.contains(checkBox.getText()));   
        }
    }

    public void addListener(ChangeListener<? super String> listener) {
        this.listeners.add(listener);
    }

    protected void onChange(ObservableValue observable, Object oldValue, Object newValue) {
        List<String> checkedList = new ArrayList<>();
        for (CheckBox checkBox : this.checkboxes) {
            if (!checkBox.isSelected()) {
                continue;
            }
            checkedList.add(checkBox.getText());
        }
        this.fireChangeEvent(observable, oldValue, checkedList);
    }

    protected void fireChangeEvent(ObservableValue observable, Object oldValue, List<String> newValue) {
        Boolean hasChanged = this.oldValue.size() != newValue.size();
        for (String value : newValue) {
            if (this.oldValue.contains(value)) {
                continue;
            }
            hasChanged = true;
        }
        if (!hasChanged) {
            return;
        }
        for (ChangeListener listener : this.listeners) {
            listener.changed(observable, oldValue, newValue);
        }
        this.oldValue = newValue;
    }
}