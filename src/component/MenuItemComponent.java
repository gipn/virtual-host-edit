package component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import facade.Provider;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import kernel.adapter.MenuItemAdapter;
import model.VirtualHostModel;
import model.VirtualHostFileModel;
import service.MenuService;
import service.ParserService;

public class MenuItemComponent extends Component<VirtualHostFileModel> {

    private TreeItem<MenuItemAdapter> treeItem;

    @Override
    protected void build() {
        this.treeItem = new TreeItem<>();
    }

    @Override
    public void update(VirtualHostFileModel model) {
        if (model == null) {
            return;
        }
        this.treeItem.setValue(model);

        List<TreeItem<MenuItemAdapter>> newItems = new ArrayList<>();
        for (VirtualHostModel vhModel : model.getChildren()) {
            newItems.add(this.findOrCreate(vhModel));
        }
        this.treeItem.getChildren().clear();
        this.treeItem.getChildren().addAll(newItems);
        
    }

    private TreeItem<MenuItemAdapter> findOrCreate(VirtualHostModel model) {
        for (TreeItem<MenuItemAdapter> item : this.treeItem.getChildren()) {
            if (item.getValue().getId() != model.getId()) {
                continue;
            }
            return item;
        }
        return new TreeItem<>(model);
    }

    @Override
    public Object render() {
        return this.treeItem;
    }
}