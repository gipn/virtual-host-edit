package component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;

public class MultichoiceAllCheckboxComponent extends MultichoiceCheckboxComponent {

    protected CheckBox allCheckbox;

    public void initialize(List<String> model, List<String> choices) {
        this.allCheckbox = this.createCheckbox("All");
        super.initialize(model, choices);
    }

    @Override
    protected void build() {
        super.build();

        this.allCheckbox.selectedProperty().addListener((ObservableValue<?> observable, Object oldValue, Object newValue) -> {
            Boolean checked = (Boolean) newValue;
            if (checked) {
                for (CheckBox ch : this.checkboxes) {
                    ch.setSelected(true);
                }
            }
            this.onChange(observable, oldValue, newValue);
        });
        container.getChildren().add(0, this.allCheckbox);
    }

    public void update(List<String> data) {
        if (data == null) {
            data = new ArrayList<>();
        }
        if (data.contains(this.allCheckbox.getText())) {
            for (String choice : this.choices) {
                data.add(choice);
            }
        }
        super.update(data);
    }

    protected void onChange(ObservableValue observable, Object oldValue, Object newValue) {
        List<String> checkedList = new ArrayList<>();
        Boolean allChecked = true;
        for (CheckBox checkBox : this.checkboxes) {
            if (checkBox.isSelected()) {
                continue;
            }
            allChecked = false;
        }
        if (!allChecked) {
            this.allCheckbox.setSelected(false);
            super.onChange(observable, oldValue, newValue);
        } else {
            this.allCheckbox.setSelected(true);
            checkedList.add(this.allCheckbox.getText());
            this.fireChangeEvent(observable, oldValue, checkedList);
        }
    }
}