package component;

import java.io.File;

import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import kernel.anotations.Service;
import service.FormService;

public class PathChooserComponent extends Component<String> {
    private TextField text;
    @Service public FormService serviceForm;

    @Override
    protected void build() {
        this.text = new TextField();
        this.container = new HBox();
        Button selectFolderButton = new Button("...");
        selectFolderButton.setOnAction((event) -> {
            File selectedFolder = this.serviceForm.openFolder("Vyber priecinok");
            if (selectedFolder != null) {
                this.text.setText("\"" + selectedFolder.getAbsolutePath() + File.separator + "\"");
            }
        });
        HBox.setHgrow(this.text, Priority.ALWAYS);
        this.container.getChildren().add(this.text);
        this.container.getChildren().add(selectFolderButton);
    }

    @Override
    public void update(String data) {
        this.text.setText(data);
    }

    public StringProperty textProperty() {
        return this.text.textProperty();
    }

    public void addListener(ChangeListener<? super String> listener) {
        this.text.textProperty().addListener(listener);
    }
}