package service;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EventService extends Service implements EventEmitter {
    private Map<String, List<EventListener>> map;

    public EventService() {
        this.map = new HashMap<>();
    }

    public void emit(String name, Object data) {
        List<EventListener> refs = this.map.get(name);
        if (refs == null) {
            return;
        }
        for (EventListener listener : refs) {
            if (listener != null) {
                listener.handle(data);
            }
        }
    }

    public void on(String name, EventListener listener) {
        if (!this.map.containsKey(name)) {
            this.map.put(name, new ArrayList<>());
        }
        List<EventListener> refs = this.map.get(name);
        refs.add(listener);
    }

    public void off(String name, EventListener listener) {
        if (!this.map.containsKey(name)) {
            return;
        }
        List<EventListener> refs = this.map.get(name);
        refs.remove(listener);
    }

    public EventEmitter createEmitter() {
        return new EventService();
    }

    public interface EventListener {
        public void handle(Object data);
    }
}