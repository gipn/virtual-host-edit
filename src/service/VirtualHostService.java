package service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kernel.anotations.Service;
import model.VirtualHostFileModel;
import model.VirtualHostModel;

public class VirtualHostService extends service.Service {
    List<VirtualHostFileModel> fileModels;

    @Service public AppStateService serviceAppState;
    @Service public EventService serviceEvent;

    public VirtualHostService() {
        this.fileModels = new ArrayList<>();
    }

    @Override
    public void initialize() {
        super.initialize();
        this.loadAppState();
        this.serviceEvent.on("window.close", (Object object) -> {
            this.storeAppState();
        });
        this.serviceEvent.emit("virtualHost.afterInitialize", null);
    }

    private void loadAppState() {
        List<Object> objectFileModels = this.serviceAppState.loadList("menu.fileModels");
        System.out.println("LOAD STATE");
        for (Object o : objectFileModels) {
            VirtualHostFileModel fileModel = (VirtualHostFileModel) o;
            this.add(fileModel);  
            System.out.println(fileModel.getFile().getAbsolutePath());
        }
    }

    private void storeAppState() {
        this.serviceAppState.store("menu.fileModels", this.fileModels);
    }

    public void save(VirtualHostModel host) {
        this.save(this.getFileModel(host));
    }

    public void save(VirtualHostFileModel fileModel) {
        //fileModel.getFile().getAbsolutePath()
        this.save(fileModel, "/home/arksys/Documents/peto/projekty/vhost-editor/src/test.conf");
    }

    public void save(VirtualHostFileModel fileModel, String fileName) {
        if (fileModel == null) {
            return;
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(fileModel.toString());
            writer.close();

            fileModel.reset();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public VirtualHostFileModel getFileModel(VirtualHostModel host) {
        for (VirtualHostFileModel fileModel : this.fileModels) {
            if (!fileModel.getChildren().contains(host)) {
                continue;
            }
            return fileModel;
        }
        return null;
    }

    public VirtualHostModel getHostModel(Integer fileId, Integer modelId) {
        VirtualHostFileModel fileModel = this.getFileModel(fileId);
        if (fileModel == null) {
            return null;
        }
        for (VirtualHostModel model : fileModel.getChildren()) {
            if (model.getId() != modelId) {
                continue;
            }
            return model;
        }
        return null;
    }

    public VirtualHostFileModel getFileModel(Integer id) {
        for (VirtualHostFileModel model : this.fileModels) {
            if (model.getId() != id) {
                continue;
            }
            return model;
        }
        return null;
    }

    public void remove(VirtualHostFileModel fileModel) {
        this.fileModels.remove(fileModel);
        this.serviceEvent.emit("virtualHost.change", this.fileModels);
    }

    public void remove(VirtualHostModel hostModel) {
        VirtualHostFileModel fileModel = this.getFileModel(hostModel);
        if (fileModel == null) {
            return;
        }
        fileModel.removeHost(hostModel);
    }

    public void add(VirtualHostFileModel fileModel) {
        this.fileModels.add(fileModel);
        this.serviceEvent.emit("virtualHost.change", this.fileModels);
    }

    public List<VirtualHostFileModel> getAll() {
        return this.fileModels;
    }

    public boolean has(File file) {
        for (VirtualHostFileModel model : this.fileModels) {
            System.out.println("CMP: " + model.getFile().getAbsolutePath() + " - " + file.getAbsolutePath());
            if (!model.getFile().equals(file)) {
                continue;
            }
            return true;
        }
        return false;
    }
}