package service;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.naming.InitialContext;

import kernel.anotations.Service;
import model.VirtualHostFileModel;
import model.VirtualHostModel;

public class MenuService extends service.Service {
    VirtualHostModel active = null;
    @Service public EventService serviceEvent;
    @Service public ParserService serviceParser;
    @Service public AppStateService serviceAppState;
    @Service public VirtualHostService serviceVirtualHost;

    public MenuService() {
        
    }

    @Override
    public void initialize() {
        this.serviceEvent.on("window.close", (Object object) -> {
            this.storeAppState();
        });
        this.serviceEvent.on("virtualHost.change", (event) -> {
            this.serviceEvent.emit("menu.change", event);
        });
        this.serviceEvent.on("virtualHost.afterInitialize", (event) -> {
            this.boot();
        });

        this.boot();
    }

    private void boot() {
        Integer activeFileId = Integer.parseInt(this.serviceAppState.load("active_file_index", "-1"));
        Integer activeModelId = Integer.parseInt(this.serviceAppState.load("active_model_index", "-1"));
        this.setActive(this.serviceVirtualHost.getHostModel(activeFileId, activeModelId));
    }

    private void storeAppState() {
        VirtualHostFileModel fileModel = this.serviceVirtualHost.getFileModel(this.active);
        Integer fileId = (fileModel != null) ? fileModel.getId() : -1;
        Integer hostId = (this.active != null) ? this.active.getId() : -1;
        this.serviceAppState.store("active_file_index", fileId.toString());
        this.serviceAppState.store("active_model_index", hostId.toString());
    }

    public void setActive(VirtualHostModel active) {
        if (this.active == active) {
            return;
        }
        this.active = active;
        this.emitActiveChange();
    }

    public VirtualHostModel getActive() {
        return this.active;
    }

    public void emitActiveChange() {
        this.serviceEvent.emit("menu.active.change", this.active);
    }

    public boolean addFile(File file) {
        if (file == null) {
            return false;
        }
        System.out.println("ADD FILE");
        System.out.println(file.getAbsolutePath());
        List<VirtualHostModel> hosts = this.serviceParser.parse(file);
        VirtualHostFileModel fileModel = new VirtualHostFileModel(file);
        fileModel.addAllHosts(hosts);
        fileModel.initialize();
        this.serviceVirtualHost.add(fileModel);
        return true;
    }

    public List<VirtualHostFileModel> getMenuItems() {
        return this.serviceVirtualHost.getAll();
    }
}