package service;

import service.EventService.EventListener;

public interface EventEmitter {
    public void emit(String name, Object data);
    public void on(String name, EventListener listener);
    public void off(String name, EventListener listener);
}