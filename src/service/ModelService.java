package service;

import kernel.anotations.Service;
import model.Model;
import util.generator.IncrementalGenerator;

public class ModelService extends service.Service {
    @Service public AppStateService serviceAppState;
    @Service public EventService serviceEvent;

    @Override
    public void initialize() {
        super.initialize();
        this.serviceEvent.on("window.close", (event) -> {
            this.serviceAppState.store("model.generator", Model.generator);
        });
        IncrementalGenerator generator = (IncrementalGenerator) this.serviceAppState.loadObject("model.generator");
        if (generator == null) {
            generator = new IncrementalGenerator();
        }
        Model.generator = generator;
    }
}