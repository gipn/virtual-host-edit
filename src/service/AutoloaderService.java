package service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import facade.Provider;
import kernel.anotations.Component;
import kernel.anotations.Service;

public class AutoloaderService extends service.Service {
    public void loadComponents(Object object) {
        Class<?> selfClass = object.getClass();
        for (Field field : selfClass.getDeclaredFields()) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Component.class)) {
                continue;
            }

            try {
                Constructor constructor =  field.getType().getConstructor();
                field.set(object, constructor.newInstance());
            } catch (NoSuchMethodException  ex) {
                System.err.println(ex.getMessage());
            } catch (InstantiationException ex) {
                System.err.println(ex.getMessage());
            } catch (IllegalAccessException ex) {
                System.err.println(ex.getMessage());
            } catch (InvocationTargetException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    public void loadServices(Object object) {
        Class<?> selfClass = object.getClass();
        for (Field field : selfClass.getFields()) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Service.class)) {
                continue;
            }
            String serviceName = this.fieldToServiceName(field);
            try {
                field.set(object, field.getType().cast(Provider.get(serviceName)));
            } catch (IllegalAccessException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    private String fieldToServiceName(Field field) {
        String fieldName = field.getName();
        String serviceName = "";
        for (char ch : fieldName.toCharArray()) {
            if (Character.isUpperCase(ch)) {
                serviceName += ".";
            }
            serviceName += Character.toLowerCase(ch);
        }
        return serviceName;
    }
}