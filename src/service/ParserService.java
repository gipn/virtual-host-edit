package service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.VirtualHostModel;
import util.parser.pattern.AllowOverride;
import util.parser.pattern.DirectoryStart;
import util.parser.pattern.DocumentRootProperty;
import util.parser.pattern.EnviromentVariable;
import util.parser.pattern.ParserPattern;
import util.parser.pattern.ServerAliasProperty;
import util.parser.pattern.ServerNameProperty;
import util.parser.pattern.VirtualHostEnd;
import util.parser.pattern.VirtualHostStart;

public class ParserService extends Service {
    private List<ParserPattern> patterns;

    public ParserService() {
        this.patterns = new ArrayList<>();

        this.addPattern(new VirtualHostStart());
        this.addPattern(new VirtualHostEnd());
        this.addPattern(new ServerAliasProperty());
        this.addPattern(new ServerNameProperty());
        this.addPattern(new DocumentRootProperty());
        this.addPattern(new EnviromentVariable());
        this.addPattern(new DirectoryStart());
        this.addPattern(new AllowOverride());
    }

    public List<VirtualHostModel> parse(File file) {
        VirtualHostBuilder builder = new VirtualHostBuilder(this.patterns, 0, 1);
        List<String> lines = this.splitIntoLines(file);
        for (String line : lines) {
            builder.addLine(line);
        }
        return builder.getHosts();
    } 

    private List<String> splitIntoLines(File file) {
        List<String> lines = new LinkedList<>();
        try {
            FileReader input = new FileReader(file);
            BufferedReader bufRead = new BufferedReader(input);
            String line = null;
            while ((line = bufRead.readLine()) != null) {    
                lines.add(line);
            }
            input.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return lines;
    }

    public void addPattern(ParserPattern patrern) {
        this.patterns.add(patrern);
    }

    public static class VirtualHostBuilder {
        private ParserPattern startPattern;
        private ParserPattern endPattern;
        private List<ParserPattern> patterns;
        private List<VirtualHostModel> models;
        private VirtualHostModel currentModel;

        public VirtualHostBuilder(List<ParserPattern> patterns) {
            this(patterns, 0);
        }

        public VirtualHostBuilder(List<ParserPattern> patterns, Integer start) {
            this(patterns, 0, patterns.size() - 1);
        }

        public VirtualHostBuilder(List<ParserPattern> patterns, Integer start, Integer end) {
            this.currentModel = new VirtualHostModel();
            this.models = new ArrayList<>();
            this.patterns = patterns;
            this.startPattern = patterns.get(start);
            this.endPattern = patterns.get(end);
        }

        public VirtualHostBuilder addLine(String line) {
            for (ParserPattern p : this.patterns) {
                if (!p.isMatch(line)) {
                    continue;
                }
                if (p == this.startPattern) {
                    this.currentModel = new VirtualHostModel();
                }
                p.setModel(line, this.currentModel); 
                if (p == this.endPattern) {
                    this.models.add(this.currentModel);
                }
                return this;
            }
            return this;
        }

        public List<VirtualHostModel> getHosts() {
            return this.models;
        }
    }
}