package service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

public class KeyboardShortcutService extends Service {
    private Map<KeyCombination, String> combinations;
    private Map<String, ShortcutAction> actions;

    public KeyboardShortcutService() {
        this.combinations = new HashMap<>();
        this.actions = new HashMap<>();
        
    }

    public void boot(Scene scene) {
        scene.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
            Iterator<KeyCombination> i = this.combinations.keySet().iterator();
            while (i.hasNext()) {
                KeyCombination comb = i.next();
                if (!comb.match(event)) {
                    continue;
                }
                this.fire(this.combinations.get(comb));
            }
        });
    }

    public void addCombination(KeyCombination combination, String action) {
        this.combinations.put(combination, action);
    }

    public void add (String actionName, KeyCombination combination, ShortcutAction shortcutAction) {
        this.addAction(actionName, shortcutAction);
        this.addCombination(combination, actionName);
    }

    public void addAction(String actionName, ShortcutAction shortcutAction) {
        this.actions.put(actionName, shortcutAction);
    }

    public void fire(String action) {
        ShortcutAction shortcutAction = this.actions.get(action);
        if (shortcutAction == null) {
            return;
        }
        shortcutAction.handle();
    }

    public interface ShortcutAction {
        public void handle();
    }
}