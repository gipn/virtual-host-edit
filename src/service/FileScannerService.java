package service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import util.filescanner.FileScanner;

public class FileScannerService extends Service implements FileScanner {
    private List<FileScanner> scanners;

    public FileScannerService() {
        this.scanners = new ArrayList<>();
    }

    public void addScanner(FileScanner scanner) {
        this.scanners.add(scanner);
    }

    @Override
    public List<File> scan() {
        List<File> files = new ArrayList<>();
        for (FileScanner scanner : this.scanners) {
            files.addAll(scanner.scan());
        }
        return files;
    }
}