package service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.chrono.MinguoChronology;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;

import facade.ArraysUtil;
import util.modelbehavior.AppStateSerializable;

public class AppStateService extends Service {

    Preferences userPreferences;

    public AppStateService() {
        this.userPreferences = Preferences.userRoot().node(this.getClass().getName());
    }

    public boolean has(String name) {
        return !this.load(name).isEmpty();
    }

    public String load(String name) {
        return this.load(name, "");
    }

    public String load(String name, String def) {
        byte[] byteArray = this.loadByteArray(name);
        if (byteArray.length == 0) {
            return def;
        }
        return new String(byteArray);
    }

    public byte[] loadByteArray(String name) {
        byte[] emptyArray = {};
        byte[] result = {};
        int index = 0;
        while (true) {
            byte[] partialResult = this.userPreferences.getByteArray(name + "-pt" + index, emptyArray);
            if (partialResult.length == 0) {
                return result;
            }
            result = ArraysUtil.join(result, partialResult);
            index++;
        }
    }

    protected AppStateSerializable deserialize(String name) {
        byte[] byteArray = this.loadByteArray(name);
        ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
        try {
            ObjectInputStream stream = new ObjectInputStream(bis);
            AppStateSerializable serializable = (AppStateSerializable) stream.readObject();
            serializable.afterDeserialize();
            return serializable;
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                bis.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return null;
    }

    public Object loadObject(String name) {
        return this.deserialize(name);
    }

    public List<Object> loadList(String name) {
        List<Object> list = new ArrayList<>();
        int i = 0;
        while (true) {
            String key = name + "." + i;
            if (!this.has(key)) {
                break;
            }
            i++;
            Object o = this.loadObject(key);
            if (o == null) {
                continue;
            }
            list.add(o);
        }
        return list;
    }

    public void store(String name, String value) {
        this.store(name, value.getBytes());
    }

    public void clearList(String name) {
        Integer i = 0;
        while (true) {
            String key = name + "." + i.toString();
            if (!this.has(key)) {
                return;
            }
            this.userPreferences.remove(key);
            i++;
        }
    }

    protected byte[] serialize(AppStateSerializable serializable) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream stream = new ObjectOutputStream(bos);
            serializable.beforeSerialize();
            stream.writeObject(serializable);
            stream.flush();
            return bos.toByteArray();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return null;
    }

    public void store(String name, List<? extends AppStateSerializable> list) {
        this.clearList(name);
        int i = 0;
        for (AppStateSerializable s : list) {
            this.store(name + "." + i, s);
            i++;
        }
    }

    public void store(String name, AppStateSerializable object) {
        this.store(name, this.serialize(object));
    }

    public void store(String name, byte[] data) {
        byte[][] chunks = ArraysUtil.chunk(data, Preferences.MAX_VALUE_LENGTH / 4 * 3);
        for (int i = 0; i < chunks.length; i++) {
            this.userPreferences.putByteArray(name + "-pt" + i, chunks[i]);
        }
    }

    public Preferences getPreferances() {
        return this.userPreferences;
    }
}