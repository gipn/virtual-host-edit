package service;

import java.util.HashMap;
import java.util.Map;

import component.Component;
import kernel.anotations.Service;

public class RouterService extends service.Service {
    public @Service EventService serviceEvent;
    private Map<String, Component> routes;
    private Component currentComponent;

    public RouterService() {
        this.currentComponent = null;
        this.routes = new HashMap<>();
    }

    public void addRoute(String name, Component component) {
        this.routes.put(name, component);
    }

    public void route(String name) {
        Component component = this.routes.get(name);
        if (component == this.currentComponent) {
            return;
        }
        this.currentComponent = component;
        this.serviceEvent.emit("router.change", this.currentComponent);
    }

    public Component getCurrent() {
        if (this.currentComponent == null) {
            this.currentComponent = this.routes.get("*");
        }
        return this.currentComponent;
    }
}