package service;

import java.io.File;

import javafx.scene.Scene;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class FormService extends Service {

    private Scene scene;

    public void boot(Scene scene) {
        this.scene = scene;
    }
    
    public File openFile(String dialogName) {
        FileChooser chooser = new FileChooser();
        chooser.setTitle(dialogName);
        return chooser.showOpenDialog(this.scene.getWindow());
    }

    public File openFolder(String dialogName) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle(dialogName);
        return chooser.showDialog(this.scene.getWindow());
    }
}