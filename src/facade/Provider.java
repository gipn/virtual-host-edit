package facade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Provider {
    protected static Map<String, Object> services = new HashMap();

    public static void register(String name, Object service) {
        Provider.services.put(name, service);
    }

    public static Object get(String name) {
        return Provider.services.get(name);
    }
}