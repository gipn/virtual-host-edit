package controller;

import java.io.File;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import service.EventService;
import service.FileScannerService;
import service.RouterService;
import component.*;
import facade.Provider;

public class HomeController implements Initializable {

    private MenuComponent menuComponent;
    // private VirtualHost virtualHostComponent;
    @FXML private BorderPane container;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        RouterService router = (RouterService) Provider.get("service.router");
        EventService eventService = (EventService) Provider.get("service.event");

        eventService.on("router.change", (object) -> {
            Component component = (Component) object;
            this.container.setCenter((Node) component.render());
            component.update();
        });

        this.menuComponent = new MenuComponent();
        this.menuComponent.initialize();
        this.container.setLeft((Node) this.menuComponent.render());
        Component centerComponent = router.getCurrent();
        this.container.setCenter((Node) centerComponent.render());
    }
}