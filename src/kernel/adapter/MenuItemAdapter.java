package kernel.adapter;

import model.VirtualHostModel;

public interface MenuItemAdapter {
    public String getMenuText();
    public Object getMenuValue();
    public int getId();
}
// public class MenuItemAdapter {
//     private String name;
//     private Object value;

//     public MenuItemAdapter(String name) {
//         this(name, null);
//     }

//     public MenuItemAdapter(String name, Object value) {
//         this.name = name;
//         this.value = value;
//     }

//     public String getName() {
//         return this.name;
//     }

//     public Object getValue() {
//         return this.value;
//     }

//     public boolean isLeaf() {
//         return this.getValue() instanceof VirtualHostModel;
//     }

//     @Override
//     public String toString() {
//         return this.getName();
//     }
// }