package kernel.adapter;

public class StringMenuItemAdapter implements MenuItemAdapter {
    private String name;

    public StringMenuItemAdapter(String name) {
        this.name = name;
    }

    @Override
    public String getMenuText() {
        return this.name;
    }

    @Override
    public Object getMenuValue() {
        return null;
    }

    @Override
    public int getId() {
        return -1;
    }
}